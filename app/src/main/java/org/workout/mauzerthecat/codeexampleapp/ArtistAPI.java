package org.workout.mauzerthecat.codeexampleapp;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.workout.mauzerthecat.codeexampleapp.artist_model.FeedArtist;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by MauzerTheCat on 05.07.2017.
 */

public interface ArtistAPI {

    @GET("{method_name}")
    Observable<JsonObject> getArtist(@Path("method_name") String method,
            @Query("page_size") int size,
            @Query("country") String country,
            @Query("apikey") String apikey );
}



