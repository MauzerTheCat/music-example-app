package org.workout.mauzerthecat.codeexampleapp.artist_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MauzerTheCat on 05.07.2017.
 */

public class ArtistList {
    @SerializedName("artist")
    @Expose
    private Artist artist;

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    @Override
    public String toString() {
        return "ArtistList{" +
                "artist=" + artist +
                '}';
    }
}
