package org.workout.mauzerthecat.codeexampleapp.track_list_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MauzerTheCat on 20.06.2017.
 */

public class Header {

    @SerializedName("available")
    @Expose
    private Integer available;

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Header{" +
                "available=" + available +
                '}';
    }
}
