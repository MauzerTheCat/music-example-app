package org.workout.mauzerthecat.codeexampleapp.track_list_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;

/**
 * Created by MauzerTheCat on 20.06.2017.
 */

public class Body {

    @SerializedName("track_list")
    @Expose
    private ArrayList<TrackList> track_list;

    public ArrayList<TrackList> getTrack_list() {
        return track_list;
    }

    public void setTrack_list(ArrayList<TrackList> track_list) {
        this.track_list = track_list;
    }

    @Override
    public String toString() {
        return "Body{" +
                "track_list=" + track_list +
                '}';
    }
}
