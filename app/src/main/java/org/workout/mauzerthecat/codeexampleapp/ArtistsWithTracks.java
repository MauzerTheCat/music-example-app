package org.workout.mauzerthecat.codeexampleapp;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;


/**
 * Created by MauzerTheCat on 13.07.2017.
 */

public class ArtistsWithTracks {

    public JsonObject feedTRack;
    public JsonObject  feedArtist;

    public JsonObject getFeedTRack() {
        return feedTRack;
    }

    public JsonObject getFeedArtist() {
        return feedArtist;
    }

    public ArtistsWithTracks(JsonObject feedTRack, JsonObject feedArtist) {
        this.feedTRack = feedTRack;
        this.feedArtist = feedArtist;

    }
}
