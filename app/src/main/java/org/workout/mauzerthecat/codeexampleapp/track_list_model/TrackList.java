package org.workout.mauzerthecat.codeexampleapp.track_list_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MauzerTheCat on 20.06.2017.
 */

public class TrackList {

    @SerializedName("track")
    @Expose
    private Track track;

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    @Override
    public String toString() {
        return "TrackList{" +
                "track=" + track +
                '}';
    }
}
