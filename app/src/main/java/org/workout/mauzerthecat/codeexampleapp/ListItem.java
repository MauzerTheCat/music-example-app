package org.workout.mauzerthecat.codeexampleapp;

import org.workout.mauzerthecat.codeexampleapp.track_list_model.Track;

import java.util.ArrayList;

/**
 * Created by MauzerTheCat on 07.07.2017.
 */

public class ListItem {
    String artistName;
    ArrayList<Track> tracks;
    int artistRating;

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public ArrayList<Track> getTracks() {
        return tracks;
    }

    public void setTracks(ArrayList<Track> tracks) {
        this.tracks = tracks;
    }

    public int getArtistRating() {
        return artistRating;
    }

    public void setArtistRating(int artistRating) {
        this.artistRating = artistRating;
    }
}
