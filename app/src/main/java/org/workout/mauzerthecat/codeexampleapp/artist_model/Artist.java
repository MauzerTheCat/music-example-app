package org.workout.mauzerthecat.codeexampleapp.artist_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MauzerTheCat on 05.07.2017.
 */

public class Artist {

    @SerializedName("artist_id")
    @Expose
    private int artist_id;

    @SerializedName("artist_name")
    @Expose
    private String artist_name;

    @SerializedName("artist_rating")
    @Expose
    private String artist_rating;

    public String getArtist_rating() {
        return artist_rating;
    }

    public void setArtist_rating(String artist_rating) {
        this.artist_rating = artist_rating;
    }

    public int getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(int artist_id) {
        this.artist_id = artist_id;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "artist_id=" + artist_id +
                ", artist_name='" + artist_name + '\'' +
                ", artist_rating='" + artist_rating + '\'' +
                '}';
    }
}
