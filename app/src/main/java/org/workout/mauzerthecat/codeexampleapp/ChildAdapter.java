package org.workout.mauzerthecat.codeexampleapp;

import static org.workout.mauzerthecat.codeexampleapp.R.mipmap.ic_launcher;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideContext;
import com.bumptech.glide.annotation.GlideType;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.workout.mauzerthecat.codeexampleapp.track_list_model.Track;

import java.util.ArrayList;

/**
 * Created by MauzerTheCat on 17.07.2017.
 */

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ViewHolder> {

    ArrayList<Track> tracksData;
    Context mContext;

    public ChildAdapter(ArrayList<Track> tracksData, Context mContext) {
        if (tracksData == null) {
            throw new IllegalArgumentException(
                    "PrescriptionProductList must not be null");
        }
        this.tracksData = tracksData;
        this.mContext = mContext;
    }

    @Override
    public ChildAdapter.ViewHolder onCreateViewHolder(
            ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_track,
                        viewGroup,
                        false);
        return new ChildAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        String nameForTrack = tracksData.get(position).getTrack_name();
        String trackAlbumUrl = tracksData.get(position).getAlbum_coverart_100x100();
        trackAlbumUrl.replaceAll("\\\\", "");
        holder.songNameTextView.setText(nameForTrack);
        holder.albumPicImageView.setVisibility(View.GONE);
        Glide.with(mContext)
                .load(trackAlbumUrl)
                .listener(new RequestListener<Drawable>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.albumPicProgressBar.setVisibility(View.GONE);
                        holder.albumPicImageView.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.albumPicProgressBar.setVisibility(View.GONE);
                        holder.albumPicImageView.setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .into(holder.albumPicImageView);
    }

    @Override
    public int getItemCount() {
        return tracksData.size();
    }

    public final static class ViewHolder
            extends RecyclerView.ViewHolder {
        TextView songNameTextView;
        ImageView albumPicImageView;
        ProgressBar albumPicProgressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            songNameTextView = (TextView) itemView.findViewById(R.id.songNameTextView);
            albumPicImageView = (ImageView) itemView.findViewById(R.id.albumPicImageView);
            albumPicProgressBar = (ProgressBar) itemView.findViewById(R.id.albumPicProgressBar);
        }
    }
}
