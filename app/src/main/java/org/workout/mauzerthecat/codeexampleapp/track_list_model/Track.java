package org.workout.mauzerthecat.codeexampleapp.track_list_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MauzerTheCat on 20.06.2017.
 */

public class Track {

    @SerializedName("artist_name")
    @Expose
    private String artist_name;

    @SerializedName("track_id")
    @Expose
    private String track_id;

    @SerializedName("track_name")
    @Expose
    private String track_name;

    @SerializedName("artist_id")
    @Expose
    private String artist_id;

    @SerializedName("album_coverart_100x100")
    @Expose
    private String album_coverart_100x100;

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public String getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(String artist_id) {
        this.artist_id = artist_id;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getAlbum_coverart_100x100() {
        return album_coverart_100x100;
    }

    public void setAlbum_coverart_100x100(String album_coverart_100x100) {
        this.album_coverart_100x100 = album_coverart_100x100;
    }

    @Override
    public String toString() {
        return "Track{" +
                "artist_name='" + artist_name + '\'' +
                ", track_id='" + track_id + '\'' +
                ", track_name='" + track_name + '\'' +
                ", artist_id='" + artist_id + '\'' +
                ", album_coverart_100x100='" + album_coverart_100x100 + '\'' +
                '}';
    }
}

