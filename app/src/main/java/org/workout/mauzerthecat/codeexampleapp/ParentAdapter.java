package org.workout.mauzerthecat.codeexampleapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by MauzerTheCat on 17.07.2017.
 */

public class ParentAdapter extends RecyclerView.Adapter<ParentAdapter.ListItemViewHolder> {
    private List<ListItem> listData;
    ChildAdapter adapter;
    Context myContext;

    public ParentAdapter(List<ListItem> listData, Context mContext) {
        if (listData == null) {
            throw new IllegalArgumentException(
                    "PrescriptionList must not be null");
        }
        this.listData = listData;
        this.myContext = mContext;
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(
            ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_artist,
                        viewGroup,
                        false);
        return new ListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(
            ListItemViewHolder viewHolder, int position) {
        ListItem item = listData.get(position);
        viewHolder.artistTextView.setText(item.getArtistName());
        if (item.getTracks() != null) {
            adapter = new ChildAdapter(item.getTracks(), myContext);
            viewHolder.recyclerViewTracks.setAdapter(adapter);
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public final static class ListItemViewHolder
            extends RecyclerView.ViewHolder {
        TextView artistTextView;
        RecyclerView recyclerViewTracks;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            artistTextView = (TextView) itemView.findViewById(R.id.artistTextView);
            recyclerViewTracks = (RecyclerView) itemView.findViewById(R.id.recyclerViewTracks);
            RecyclerView.LayoutManager layoutManager = new CustomLinearLayoutManager(itemView.getContext());
            recyclerViewTracks.setLayoutManager(layoutManager);
        }
    }
}