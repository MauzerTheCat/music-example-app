package org.workout.mauzerthecat.codeexampleapp;


import com.google.gson.JsonObject;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by MauzerTheCat on 20.06.2017.
 */

public interface TrackAPI {

    @GET("chart.tracks.get")
    Observable<JsonObject> getTracks(
            @Query("page_size") int size,
            @Query("country") String country,
            @Query("apikey") String apikey);
}

