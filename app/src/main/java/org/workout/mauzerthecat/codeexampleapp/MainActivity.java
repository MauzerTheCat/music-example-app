package org.workout.mauzerthecat.codeexampleapp;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.workout.mauzerthecat.codeexampleapp.artist_model.ArtistList;
import org.workout.mauzerthecat.codeexampleapp.artist_model.FeedArtist;
import org.workout.mauzerthecat.codeexampleapp.track_list_model.Track;
import org.workout.mauzerthecat.codeexampleapp.track_list_model.TrackList;
import org.workout.mauzerthecat.codeexampleapp.track_list_model.FeedTRack;

import java.util.ArrayList;


import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    String BASE_URL = "http://api.musixmatch.com/ws/1.1/";
    private static final String TAG = "MAinActivity";
    ArrayList<ListItem> dataToShow = new ArrayList<>();
    ArrayList<ArtistList> artistList = new ArrayList<>();
    ArrayList<TrackList> trackList;
    ArtistsWithTracks data;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Observable<JsonObject> trackObservable = retrofit
                .create(TrackAPI.class)
                .getTracks(100, "rus", "2defc37982c73c7d1939b70ed986c668")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        Observable<JsonObject> artistObservable = retrofit
                .create(ArtistAPI.class)
                .getArtist("chart.artists.get", 100, "rus", "2defc37982c73c7d1939b70ed986c668")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        Observable<ArtistsWithTracks> combined = Observable.zip(trackObservable, artistObservable, new Func2<JsonObject, JsonObject, ArtistsWithTracks>() {
            @Override
            public ArtistsWithTracks call(JsonObject jsonObject, JsonObject jsonObject2) {
                data = new ArtistsWithTracks(jsonObject, jsonObject2);
                return data;
            }

        });

        combined.subscribe(new Subscriber<ArtistsWithTracks>() {
            @Override
            public void onCompleted() {
                for (int i = 0; i < dataToShow.size(); i++) {
                    Log.d(TAG, "NAME: " + dataToShow.get(i).getArtistName());
                    if (dataToShow.get(i).getTracks() != null) {
                        for (int j = 0; j < dataToShow.get(i).getTracks().size(); j++) {
                            Log.d(TAG, "TRACK: " + dataToShow.get(i).getTracks().get(j));
                        }
                    }
                }

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                ParentAdapter adapter = new ParentAdapter(dataToShow, context);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(layoutManager);
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(ArtistsWithTracks artistsWithTracks) {
                Gson gson = new Gson();
                FeedTRack trackResponse = gson.fromJson(data.getFeedTRack().toString(), FeedTRack.class);
                FeedArtist artistResponse = gson.fromJson(data.getFeedArtist().toString(), FeedArtist.class);
                trackList = trackResponse.getMessage().getBody().getTrack_list();
                artistList = artistResponse.getMessage().getBody().getArtist_list();
                for (int i = 0; i < artistList.size(); i++) {
                    ListItem item = new ListItem();
                    String nameOfArtist = artistList.get(i).getArtist().getArtist_name();
                    ArrayList<Track> listForTracks = new ArrayList<Track>();
                    for (int i1 = 0; i1 < trackList.size(); i1++) {
                        if (nameOfArtist.equals(trackList.get(i1).getTrack().getArtist_name())) {
                            listForTracks.add(trackList.get(i1).getTrack());
                        }
                    }
                    item.setArtistName(nameOfArtist);
                    item.setTracks(listForTracks);
                    dataToShow.add(item);
                }
            }
        });
    }
}
