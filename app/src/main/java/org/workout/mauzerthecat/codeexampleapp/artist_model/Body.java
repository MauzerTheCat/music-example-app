package org.workout.mauzerthecat.codeexampleapp.artist_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



import java.util.ArrayList;

/**
 * Created by MauzerTheCat on 05.07.2017.
 */

public class Body {
    @SerializedName("artist_list")
    @Expose
    private ArrayList<ArtistList> artist_list;

    public ArrayList<ArtistList> getArtist_list() {
        return artist_list;
    }

    public void setArtist_list(ArrayList<ArtistList> artist_list) {
        this.artist_list = artist_list;
    }

    @Override
    public String toString() {
        return "Body{" +
                "artist_list=" + artist_list +
                '}';
    }
}
