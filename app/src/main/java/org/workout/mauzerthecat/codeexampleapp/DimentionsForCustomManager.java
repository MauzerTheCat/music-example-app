package org.workout.mauzerthecat.codeexampleapp;

/**
 * Created by MauzerTheCat on 19.07.2017.
 */

public class DimentionsForCustomManager {
    int width = 0;
    int height = 0;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
