package org.workout.mauzerthecat.codeexampleapp.artist_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MauzerTheCat on 20.06.2017.
 */

public class FeedArtist {

    @SerializedName("message")
    @Expose
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "FeedArtist{" +
                "message=" + message +
                '}';

    }
}
